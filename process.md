
composer require karser/karser-recaptcha3-bundle
--------------------------------------------------------------
----- user install
--------------------------------------------------------------
composer require symfonycasts/verify-email-bundle symfony/mailer 
symfony console make:registration-form
composer require form validator

symfony console make:auth
composer require twig

symfony console make:user
composer require security
composer require symfony/maker-bundle --dev

--------------------------------------------------------------
----- base install
--------------------------------------------------------------
yarn add sass --dev
yarn add @popperjs/core
yarn add bootstrap
yarn add file-loader
yarn add sass-loader
yarn add @symfony/webpack-encore --dev
nvm use 16.10.0
nvm install 16.10.0
yarn add @symfony/webpack-encore --dev
composer require symfony/webpack-encore-bundle
yarn install
symfony console d:d:c
composer require symfony/orm-pack
composer create-project symfony/skeleton:"6.3.*" new-nsdm  // no --webapp