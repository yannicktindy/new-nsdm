<?php

namespace App\Form;

use App\Entity\Kinkster;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Symfony\Config\KarserRecaptcha3Config;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('pseudo', TextType::class, [
            'label' => 'Pseudo',
            'label_attr' => [
                'class' => 'myLabel',
            ],
            'required' => true,
            'attr' => [
                'placeholder' => 'Pseudo',
                'class' => 'form-control',
            ],
            'constraints' => [
                new Length([
                    'min' => 4,
                    'minMessage' => 'Your pseudo should be at least {{ limit }} characters',
                    // max length allowed by Symfony for security reasons
                    'max' => 12,
                    'maxMessage' => 'Your pseudo should be at least {{ limit }} characters',
                ]),
                new NotBlank([
                    'message' => 'Please enter a pseudo',
                ]),

            ],
        ])
            // ->add('email')
            ->add('email', EmailType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 12,
                        'minMessage' => 'Your email should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 254,
                        'maxMessage' => 'Your email should be at least {{ limit }} characters',
                    ]),
                ],
            ]) 
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Agree to Insidometer\'s terms',
                'label_attr' => [
                    'class' => 'myLabel',
                ],
                'mapped' => false,
                'attr' => [
                    'class' => 'form-check-input',
                ],
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                        'maxMessage' => 'Your password should be at least {{ limit }} characters',
                    ]),
                ],
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => [
                    'label' => 'Password', 
                    'label_attr' => [
                        'class' => 'myLabel',
                    ],
                    'attr' => [
                        'placeholder' => 'Password',
                        'class' => 'form-control',
                    ],
                ],
                'second_options' => [
                    'label' => 'Repeat Password',
                    'label_attr' => [
                        'class' => 'myLabel',
                    ],
                    'attr' => [
                        'placeholder' => 'Repeat Password',
                        'class' => 'form-control',
                    ],
                ],
            ])
            
            // ->add('recaptcha', Recaptcha3Type::class, [
            //     'action_name' => 'registration', // nom d'action

            //     'constraints' => new Recaptcha3([
            //     'message' => 'Invalid captcha',
            //     ]),
            // ])
            // ->add('recaptcha', Recaptcha3Type::class, [
            //     'constraints' => [
            //         new KarserRecaptcha3Config([
            //             'message' => 'The captcha is invalid.',
            //         ]),
            //     ],
            // ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Kinkster::class,
        ]);
    }
}
