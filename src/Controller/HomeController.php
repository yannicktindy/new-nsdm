<?php

namespace App\Controller;

use App\Repository\KinksterRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'app_home')]
    public function index(
        KinksterRepository $kinksterRepository,
    ): Response
    {

        $credential = $this->getUser()->getUserIdentifier();
        $kinkster = $kinksterRepository->findOneBy(['email' => $credential]);
        $isFR = $kinkster->isIsFr();
      

        if ($kinkster->isIsFr()) {
        // la valeur de isFr
        }

        return $this->render('home/homefr.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
