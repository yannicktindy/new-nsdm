<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class StartController extends AbstractController
{
    #[Route('/', name: 'app_start_enter')]
    public function index(
        SessionInterface $session,
    ): Response
    {
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }

        return $this->render('start/enter.html.twig', [
            'controller_name' => 'StartController',
        ]);
    }

    #[Route('/start/{lang}', name: 'app_start_lang')]
    public function lang(
        string $lang,
        SessionInterface $session,
    ): Response
    {
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }

        $session->set('lang', $lang);
        return $this->redirectToRoute('app_start_present');
    }

    #[Route('/start/present', name: 'app_start_present')]
    public function present(
        SessionInterface $session,
    ): Response
    {
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }

        $lang = $session->get('lang');
        if ($lang == 'fr') {
            return $this->render('start/presentfr.html.twig', [
                'controller_name' => 'StartController',
            ]);
        } 
        
        return $this->render('start/present.html.twig', [
            'controller_name' => 'StartController',
        ]);

        //--------------------------------------------------------------------------------
        // exemple, à adapter à votre application
        // // controllers/StartController
        // public function language(Request $request) {
        //     $language = $request->request->get('language');
        //     $this->session->set('language', $language);
        //   }
        
        //   // LangManager
        //   public function getLanguageFromSession() {
        //     return $this->session->get('language', LANG_FR); 
        //   }
        
        //   public function getTemplateForLanguage() {
        //     if ($this->getLanguageFromSession() === LANG_FR) {
        //       return 'start/fr.html.twig';
        //     } else {
        //       return 'start/en.html.twig';
        //     }
        //   }


        //--------------------------------------------------------------------------------
        // methode à passer par un service kernel.event_subscriber pour éviter de répéter le code
        // public function onKernelResponse(ResponseEvent $event)  
        // {
        //     $language = $this->session->get('language');

        //     if ($language === LANG_FR) {
        //         $event->setResponse(
        //         $event->getResponse()->setTemplate('start/fr.html.twig') 
        //         );
        //     }
        // }
        //--------------------------------------------------------------------------------


    }

}
